import os
import shutil

from PIL import Image
import numpy as np

def gethue(RGB,images):
    red_only = []
    for rgb in RGB:
        min = np.min(rgb)
        max = np.max(rgb)
        if min == max:
            hue = 0
        elif max == rgb[0]:
            if rgb[1] >= rgb[2]:
                hue = ((rgb[1] - rgb[2]) / (max - min)) * 60
            else:
                hue = (((rgb[1] - rgb[2]) / (max - min)) * 60) + 360
        elif max == rgb[1]:
            hue = (((rgb[2] - rgb[0]) / (max - min)) * 60) + 120
        elif max == rgb[2]:
            hue = (((rgb[0] - rgb[2]) / (max - min)) * 60) + 240
        if (hue >= 0 and hue <= 15) or (hue <= 360 and hue >= 345):
            red_only.append('red')
        else:
            red_only.append('not_red')
    sorting(red_only,images)

def imagestoRGB():
        images = []
        RGB = np.array([[]]).reshape(0,3)
        for image in os.listdir('test'):
            images.append(image)
            picture = Image.open("test" + "\\" + '{}'.format(image) )
            picture = picture.resize((1,1))
            pixel = picture.load()
            RGB = np.vstack([RGB,pixel[0,0]])
        gethue(RGB,images)

def sorting(red_only,images):
    if (os.path.exists("Output") == False):
        os.mkdir("Output")
    for bool in range(len(red_only)):
        if red_only[bool] == 'red':
            shutil.copy('test/{}'.format(images[bool]),'Output/{}'.format(images[bool]))
        else:
            pass

imagestoRGB()

